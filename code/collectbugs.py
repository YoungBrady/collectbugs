from py2neo import Graph, Node, Relationship
from github import Issue, Github
from neo4j import *
import time

# 当次数达到限制时，程序挂起，直到速度限制重置


def wait_ratelimit_reset(rate_limiting_resettime):
    secs = rate_limiting_resettime-time.time()
    reset_time = time.strftime(
        "%Y-%m-%d %H:%M:%S", time.localtime(g.rate_limiting_resettime))
    print("reset_time:"+reset_time)
    print("sleep start……", "please wait" +
          str(secs+60)+'seconds', sep='\n')
    time.sleep(secs+60)
    print("sleep over……")


def get_repo_issues(dir: str, g: Github, graph: Graph):
    Dir = dir.split(sep='/')[0]
    comments = ""
    repo = g.get_repo(dir)  # 获得仓库
    issue_list = repo.get_issues(state='closed')  # 获得所有状态为关闭的issue
    begin = 0  # 当中途因为意外程序终止时可以设置begin的值使得程序可以在上一次终止的位置继续执行
    for i in range(begin, issue_list.totalCount):
        issue = issue_list[i]
        comments = ""
        for var in issue.get_comments():  # 将所有comment合并成一个字符串
            comments += var.body
        create_issue_node(graph, issue, comments, Dir)
        print("create "+str(i+1)+" nodes successfully")


def create_issue_node(graph: Graph, issue: Issue, comments: str, dir: str):
    # 如果直接用.访问，当相应的成员为空时会出错，因此进行下面的处理
    lable = ""
    for var in issue.labels:
        lable += var.name

    if issue.assignee != None:
        Assignee = issue.assignee.login
    else:
        Assignee = None
    if issue.user != None:
        User = issue.user.login
    else:
        User = None
    if issue.milestone != None:
        MileStone = issue.milestone.title
    else:
        MileStone = None
    if issue.closed_by != None:
        Closedby = issue.closed_by.login
    else:
        Closedby = None
    # 这里以正常时间的格式存入，如果需要时间的计算，可以使用time.time()获得当前时间戳
    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    # print(time_str)
    tx = graph.begin()
    a = Node('issue',
             repo=dir,
             id=issue.id,
             number=issue.number,
             assignee=Assignee,
             body=issue.body,
             closed_at=issue.closed_at,
             closed_by=Closedby,
             created_at=issue.created_at,
             labels=lable,
             milestone=MileStone,
             state=issue.state,
             title=issue.title,
             user=User,
             locked=issue.locked,
             active_lock_reason=issue.active_lock_reason,
             last_modified=issue.last_modified,
             comments_num=issue.comments,
             comments=comments,
             create_time=time_str)
    # print(a)
    flag = 0
    tx.create(a)
    ratelimit = g.rate_limiting
    if ratelimit[0] < 2:
        graph.commit(tx)
        flag = 1
        wait_ratelimit_reset(g.rate_limiting_resettime)
    issue_comments_lists = issue.get_comments()
    # for comment in issue_comments_lists:
    for i in range(issue_comments_lists.totalCount):
        comment = issue_comments_lists[i]
        b = Node("issue_comment", body=comment.body,
                 created_at=comment.created_at, id=comment.id, user=comment.user.login)
        ab = Relationship(a, 'comment', b)
        tx.create(ab)
        flag = 0
        ratelimit = g.rate_limiting
        if ratelimit[0] < 2:
            graph.commit(tx)
            flag = 1
            wait_ratelimit_reset(g.rate_limiting_resettime)
        reply_list = comment.get_reactions()
        # for reply in reply_list:
        for j in range(reply_list.totalCount):
            reply = reply_list[j]
            c = Node('comment_reply', content=reply.content,
                     created_at=reply.created_at, id=reply.id, user=reply.user.login)
            bc = Relationship(b, 'reply', c)
            tx.create(bc)
            flag = 0
            ratelimit = g.rate_limiting
            if ratelimit[0] < 2:
                graph.commit(tx)
                flag = 1
                wait_ratelimit_reset(g.rate_limiting_resettime)
    if(flag == 0):
        graph.commit(tx)


g = Github('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')

uri = "neo4j://localhost:7687"
graph = Graph(uri, auth=('neo4j', 'passwd'))

dir_list = ['tensorflow/federated', 'FederatedAI/FATE',
            'PaddlePaddle/PaddleFL', 'OpenMined/PySyft']

for i in range(len(dir_list)):
    get_repo_issues(dir_list[i], g, graph)
#get_repo_issues('OpenMined/PySyft', g, graph)
