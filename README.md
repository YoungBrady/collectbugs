
# 一、数据格式（数据库结点属性说明）

&emsp;&emsp;根据GitHub中issue的结构，我选择了以下几个字段作为neo4j数据库结点的属性（以`issue`为标签）：  

## **<center>issue结点的属性说明</center>**   
| 字段 | 含义 |
| ------ | ------ |
|repo|仓库名|
| id | issue的id值，每个issue有唯一id |
| number | issue的编号，和pull request共享，因此建立的数据库中包含了pull request的内容 |
| assignee|issue 的assignee的登录名|
| body |issue的具体内容|
|create_at|issue的创建时间|
|closed_at|issue的关闭时间|
|last_modified|issue的最后修改时间|
|closed_by|关闭issue的用户的登录名|
| labels | issue的标签，若有多个标签，合并成一个字符串存入|
|milestone|issue的milestone的标题|
|state|issue的状态，收集的issue的状态均为closed|
|user|issue的创建者|
|locked|issue的锁定状态|
|active_lock_reason|issue激活或锁定的原因|
|comments_num|issue的评论的数量|
|comments|issue的评论的内容，多条评论合并成一个字符串存入|
|create_time|该结点的创建时间|

&emsp;&emsp;此外，为了记录issue和comments、comments和reply之间的关系，对每个comment和reply单独建立结点，分别使用comment边和reply边记录他们之间的关系   

&emsp;&emsp;对于comment，主要记录以下几个字段，以`issue_comment`为标签   
## **<center>comment结点的属性说明</center>**  

| 字段 | 含义 |
| ------ | ------ |
| id | comment的id值，每个comment有唯一id |
| body |comment的具体内容|
|created_at|comment的创建时间|
|user|comment的创建者的登录名|


&emsp;&emsp;类似的，对reply，记录以下字段，以`comment_reply`作标签    
## **<center>comment_reply结点的属性说明</center>**  
| 字段 | 含义 |
| ------ | ------ |
| id | reply的id值，每个reply有唯一id |
| content |reply的具体内容|
|created_at|reply的创建时间|
|user|reply的创建者的登录名|
***
针对四个框架定义了4个结点，每个结点只有类型名称不同，属性相同
| 仓库 | 收集的数据数量 |
| ------ | ------ |
| issue_tensorflow | 1603 |
|issue_FederatedAI|2603|
|issue_PaddlePaddle|179|
| issue_OpenMined | 5577 |

# 二、数据查询

&emsp;&emsp;代码抓取的数据包含pull request，如果不需要pull request 可以根据pull_request字段是否为空来区分

&emsp;&emsp;可以使用关键字对数据进行简单的筛选，以`crash`为例，查询命令如下
```
match(a:issue)where a.body contains 'crash' 
match(b:issue)-[r1:comment]->(c:issue_comment)where c.body contains 'crash'
match(e:issue)-[r1:comment]->(f:issue_comment)-[r2:reply]->(g:commnet_reply)where g.content contains 'crash'
return distinct a,b,e
```
&emsp;&emsp;neo4j使用cypher语法进行数据查询
[cypher语法参照](https://www.cnblogs.com/ljhdo/p/10911426.html)

## PS:数据库导入导出
### 如果使用neo4j community版本

&emsp;&emsp;在NEO4J_HOME/bin下执行以下指令导出数据库,其中`neo4j`是数据库名，`target_file_path`是目标文件的路径    

        neo4j-admin dump --database=neo4j --to=taeget_file_path

&emsp;&emsp;导入数据库的指令：   

        neo4j-admin load --from=sourcefile_path --database=new_database_name 
&emsp;&emsp;然后在conf文件中将`dbms.default_database`修改为新的数据库名即可使用新的数据库   

### 使用桌面版neo4j会简单很多
&emsp;&emsp;直接在菜单选项中选择导入导出即可
